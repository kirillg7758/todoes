export interface IEnvironment {
  production: boolean;
  databaseUri: string
}
