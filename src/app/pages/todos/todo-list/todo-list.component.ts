import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ICreateTodo, ITodo} from "../../../shared/interfaces/todo.interface";
import {TodosService} from "../todos.service";
import {FlipperComponent} from "../../../shared/components/flipper/flipper.component";

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {
  @ViewChild('flipper') flipper!: FlipperComponent;

  @Input() todos: ITodo[] = [];

  @Output() creteTodo = new EventEmitter<ICreateTodo>();
  @Output() completeTodo = new EventEmitter<string>();
  @Output() deleteTodo = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {}

  onCreate(todo: ICreateTodo) {
    this.flipper.flipToFront();
    this.creteTodo.emit(todo);
  }

  onComplete(id: string) {
    this.completeTodo.emit(id);
  }

  onDelete(id: string) {
    this.deleteTodo.emit(id);
  }

}
