import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TodosRoutingModule } from './todos-routing.module';
import { TodoListComponent } from './todo-list/todo-list.component';
import { TodoComponent } from './todo/todo.component';
import { TodosComponent } from './todos.component';
import {MatCardModule} from "@angular/material/card";
import {MatButtonModule} from "@angular/material/button";
import {MatMenuModule} from "@angular/material/menu";
import {MatIconModule} from "@angular/material/icon";
import {FlipperComponent} from "../../shared/components/flipper/flipper.component";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import { EditTodoComponent } from './edit-todo/edit-todo.component';
import {ReactiveFormsModule} from "@angular/forms";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import { CreateTodoComponent } from './create-todo/create-todo.component';
import { TodoActionsComponent } from './todo-actions/todo-actions.component';
import {MatSelectModule} from "@angular/material/select";


@NgModule({
  declarations: [
    TodoListComponent,
    TodoComponent,
    TodosComponent,
    FlipperComponent,
    EditTodoComponent,
    CreateTodoComponent,
    TodoActionsComponent
  ],
  imports: [
    CommonModule,
    TodosRoutingModule,
    MatCardModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatSelectModule
  ]
})
export class TodosModule { }
