import {Component, OnDestroy, OnInit} from '@angular/core';
import {ICreateTodo, ITodo} from "../../shared/interfaces/todo.interface";
import {TodosService} from "./todos.service";
import {delay, merge, Subject, takeUntil} from "rxjs";

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss']
})
export class TodosComponent implements OnInit, OnDestroy {
  todos: ITodo[] = [];
  filteredTodos: ITodo[] = [];
  isLoading = true;

  private destroy$ = new Subject();

  constructor(
    private todosService: TodosService
  ) { }

  ngOnInit(): void {
    this.todosService.observer
      .pipe(takeUntil(this.destroy$)).subscribe(todos => {
        this.filteredTodos = this.todos = todos;
        this.isLoading = false;
      }, (err) => {
        console.error(err);
        this.isLoading = false;
      })
  }

  onCreate(todo: ICreateTodo) {
    this.todosService.createTodo(todo.title, todo.description).subscribe();
  }

  onComplete(id: string) {
    this.todosService.completeTodo(id).subscribe();
  }

  onMarkAllAsCompleted() {
    this.todos.forEach(todo => {
      this.onComplete(todo.id);
    })
  }

  onDelete(id: string) {
    this.todosService.deleteTodo(id)
      .subscribe();
  }

  onDeleteAll() {
    const deleteSubscriptions = this.todos.map((todo, idx) =>
      this.todosService.deleteTodo(todo.id)
      .pipe(delay(idx * 1000)));
    merge(...deleteSubscriptions).subscribe();
  }

  onDeleteAllCompleted() {
    const allCompletedTodos = this.todos.filter(todo => todo.completed);
    allCompletedTodos.forEach(todo => {
      this.onDelete(todo.id);
    });
  }

  filter(filterState: { title: string, status?: boolean }) {
    this.filteredTodos = this.todos.filter(todo => {
      const isFilterByStatus = typeof filterState.status === "boolean";

      if (isFilterByStatus && filterState.title) {
        return todo.title.includes(filterState.title) && todo.completed === filterState.status;
      } else if (isFilterByStatus && !filterState.title) {
        return todo.completed === filterState.status;
      }

      return todo.title.includes(filterState.title);
    })
  }

  ngOnDestroy() {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}
