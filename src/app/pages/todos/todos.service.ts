import { Injectable } from '@angular/core';
import {ITodo} from "../../shared/interfaces/todo.interface";
import {map, Observable, tap, switchMap, BehaviorSubject} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";

const TODOS: ITodo[] = [
  {
    id: 'todo-1',
    title: 'Todo 1',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.\n' +
      '      Accusantium at delectus dicta, dolorem ducimus eligendi facilis\n' +
      '      fugiat minus molestiae nemo perferendis possimus, quod ratione sequi,\n' +
      '      tempore! Libero quis reiciendis ut?',
    completed: false
  },
  {
    id: 'todo-2',
    title: 'Todo 2',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.\n' +
      '      Accusantium at delectus dicta, dolorem ducimus eligendi facilis\n' +
      '      fugiat minus molestiae nemo perferendis possimus, quod ratione sequi,\n' +
      '      tempore! Libero quis reiciendis ut?',
    completed: true
  },
]

// Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium at delectus dicta, dolorem ducimus eligendi facilis fugiat minus molestiae nemo perferendis possimus, quod ratione sequi, tempore! Libero quis reiciendis ut?

interface IFirebaseResponse {
  name: string
}

@Injectable({
  providedIn: 'root'
})
export class TodosService {
  private todos$ = new BehaviorSubject<ITodo[]>([]);
  private todos: ITodo[] = [];

  get observer(): Observable<ITodo[]> {
    return this.todos$.asObservable()
      .pipe(
        switchMap(() => this.getTodos()),
        map(todos => todos.sort((todoA, todoB) => (todoA.completed === todoB.completed) ? 0 : todoA.completed ? -1 : 1)),
        tap(todos => (this.todos = todos))
      )
  }

  constructor(
    private http: HttpClient
  ) { }

  getTodos(): Observable<ITodo[]> {
    return this.http.get<ITodo[]>(environment.databaseUri + 'todos.json')
      .pipe(
        map(todos => !!todos ? Object.entries(todos)
          .map(([key, value]) => ({ ...value, id: key })) : []
        )
      );
  }

  createTodo(title: string, description: string): Observable<IFirebaseResponse> {
    const url = environment.databaseUri + 'todos.json'
    return this.http.post<IFirebaseResponse>(url, { title, description, completed: false })
      .pipe(tap(createdTodo => {
        console.info('Create TODO:', createdTodo);
        const todo = {
          title,
          description,
          completed: false,
          id: createdTodo.name
        }
        this.todos$.next([...this.todos, todo]);
      }));
  }

  editTodo(id: string, title: string, description: string): Observable<IFirebaseResponse> {
    const body = { title, description };
    const url = environment.databaseUri + `todos/${id}.json`
    return this.http.patch<IFirebaseResponse>(url, body)
      .pipe(
        tap((updatedTodo) => {
          console.info('Updated TODO:', updatedTodo)
          const todos = this.todos.map((todo) => {
            if (todo.id === updatedTodo.name) {
              return {
                id: updatedTodo.name,
                completed: todo.completed,
                title,
                description
              }
            }
            return todo;
          })
          this.todos$.next(todos);
        })
      );
  }

  deleteTodo(id: string): Observable<null> {
    const url = environment.databaseUri + `todos/${id}.json`
    return this.http.delete<null>(url)
      .pipe(
        tap((deletedTodo) => {
          console.info('Deleted TODO:', deletedTodo)
          const todos = this.todos.filter((todo) => todo.id !== id);
          this.todos$.next(todos);
        })
      );
  }

  completeTodo(id: string): Observable<IFirebaseResponse> {
    const body = { completed: true };
    const url = environment.databaseUri + `todos/${id}.json`
    return this.http.patch<IFirebaseResponse>(url, body)
      .pipe(
        tap((updatedTodo) => {
          console.info('Updated TODO:', updatedTodo);
          const todos = this.todos.map((todo) => {
            if (todo.id === updatedTodo.name) {
              return {
                ...todo,
                id: updatedTodo.name,
                completed: true,
              }
            }
            return todo;
          })
          this.todos$.next(todos);
        })
      );
  }
}
