import {Component, Input, OnInit} from '@angular/core';
import {ITodo} from "../../../shared/interfaces/todo.interface";
import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";
import {TodosService} from "../todos.service";

@Component({
  selector: 'app-edit-todo[todo]',
  templateUrl: './edit-todo.component.html',
  styleUrls: ['./edit-todo.component.scss']
})
export class EditTodoComponent implements OnInit {
  @Input() todo!: ITodo;
  form: FormGroup = new FormGroup({});
  isSaving = false;

  get isValid(): boolean {
    return this.form.valid;
  }

  get titleControl(): AbstractControl {
    return this.form.controls['title'];
  }

  get descriptionControl(): AbstractControl {
    return this.form.controls['description'];
  }

  constructor(
    private todosService: TodosService
  ) {}

  ngOnInit(): void {
    this.form = new FormGroup({
      title: new FormControl(this.todo.title, [Validators.required]),
      description: new FormControl(this.todo.description, [Validators.required]),
    })
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }
    this.isSaving = true;

    const { title, description } = this.form.value;
    this.todosService.editTodo(this.todo.id, title, description)
      .subscribe(res => {
        this.isSaving = false;
      }, () => {
        this.isSaving = false;
      });
  }
}
