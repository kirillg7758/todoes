import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterPipe } from './filter.pipe';
import { SortPipe } from './sort.pipe';



@NgModule({
  declarations: [
    FilterPipe,
    SortPipe
  ],
  imports: [
    CommonModule
  ]
})
export class PipesModule { }
